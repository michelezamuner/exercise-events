#ifndef SM_H
#define SM_H

#include <stdint.h>

typedef struct _event
{
  uint16_t sig;
} event;

typedef struct _sm
{
  void (*dispatch)(struct _sm *, const event *);
  uint16_t state;
} sm;

void sm_ctor (sm *, void (*dispatch)(sm *, const event *));
void sm_dispatch (sm *, const event *);

#endif
