#ifndef EVTMNG_H
#define EVTMNG_H

#include "../signals.h"
#include "sm.h"
#include "../data/vector/vector.h"
#include "../data/queue/queue.h"

typedef struct _evtMng
{
  vector machines;				/* macchine gestite dal manager */
  queue events;					/* coda di eventi */
} evtMng;

void evtMng_ctor (evtMng *);			/* costruttore */

uint16_t evtMng_totSm (const evtMng *);		/* numero totale di macchine */
void evtMng_pushSm (evtMng *, const sm **);	/* aggiungi una macchina */
sm * evtMng_getSm (const evtMng *, uint16_t);	/* ottieni una macchina */

bool evtMng_isEvt (const evtMng *);		/* c'è un evento in coda? */
void evtMng_pushEvt (evtMng *, const event **);	/* aggiungi un evento */
event * evtMng_popEvt (evtMng *);		/* consuma un evento */

void evtMng_start (evtMng *);			/* avvia la gestione degli eventi */

#endif