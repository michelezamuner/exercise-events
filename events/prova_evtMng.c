#include "evtMng.h"
#include "../ui/ui.h"

#include <stdio.h>

static evtMng *mng;
static ui *intf;

int main ()
{
  mng = malloc (sizeof (evtMng));
  evtMng_ctor (mng);
  
  intf = malloc (sizeof (ui));
  ui_ctor (intf, mng);
  
  evtMng_pushSm (mng, (const sm **)&intf);
  
  evtMng_start (mng);
  
  return 0;
}
