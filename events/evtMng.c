#include "evtMng.h"

void evtMng_ctor (evtMng * this)
{
  vector_ctor (&(this->machines), sizeof (sm *));
  queue_ctor (&(this->events), sizeof (event *));
}

uint16_t evtMng_totSm (const evtMng *this)
{
  return vector_size (&(this->machines));
}

void evtMng_pushSm (evtMng *this, const sm **m)
{
  vector_push (&(this->machines), m);
}

sm * evtMng_getSm (const evtMng *this, uint16_t index)
{
  return *(sm **)vector_get (&(this->machines), index);
}

bool evtMng_isEvt (const evtMng *this)
{
  return !queue_isEmpty (&(this->events));
}

void evtMng_pushEvt (evtMng *this, const event **e)
{
  queue_push (&(this->events), e);
}

event * evtMng_popEvt (evtMng *this)
{
  return *(event **)queue_pop (&(this->events));
}

/* l'oggetto Event Manager deve poter riconoscere gli eventi generati
 * all'esterno del programma, ad esempio le pressioni dei tasti della
 * tastiera, ed inviarli alle macchine a stati */
void evtMng_start (evtMng *this)
{
  for (;;)
  {
    /* cerchiamo eventuali eventi esterni */
    static const event _e = { QUERY_EVENT_SIG }; static const event *e = &_e;
    evtMng_pushEvt (this, &e);
    
    if (evtMng_isEvt (this))
    { 
      int tot_sm = evtMng_totSm (this);
      const event *e = evtMng_popEvt (this);
      
      int i;
      for (i = 0; i < tot_sm; i++)
      {
	sm_dispatch (evtMng_getSm (this, i), e);
      }
    }
  }
}