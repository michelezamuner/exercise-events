#include "sm.h"

void sm_ctor (sm * this, void (*dispatch)(sm *, const event *))
{ 
  this->dispatch = dispatch;
}

void sm_dispatch (sm *this, const event *e)
{
  this->dispatch(this, e);
}
