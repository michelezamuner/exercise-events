#ifndef SIGNALS_H
#define SIGNALS_H

typedef enum signals
{
  QUERY_EVENT_SIG,		/* chiede all'interfaccia di rilevare eventi esterni */
  
  N_SIG,			/* pressione del tasto 'n' */
  P_SIG,			/* pressione del tasto 'p' */
  W_SIG,			/* pressione del tasto 'w' */
  A_SIG,			/* pressione del tasto 'a' */
  S_SIG,			/* pressione del tasto 's' */
  D_SIG,			/* pressione del tasto 'd' */
  Q_SIG,			/* pressione del tasto 'q' */
  Y_SIG,			/* pressione del tasto 'y' */
  
  EXIT_SIG,			/* richiede l'uscita dall'applicazione */
  EXIT_CONFIRMED_SIG,		/* la richiesta di uscita è stata confermata */
  NEW_SIG,			/* chiede di iniziare una nuova partita */
  SELECT_SIG,			/* chiede di selezionare un'altra tessera */
  INVALID_SELECTION_SIG,	/* la selezione richiesta è invalida */
  MOVE_SIG,			/* chiede di spostare la tessera selezionata */
  INVALID_MOVE_SIG,		/* lo spostamento richiesto è invalido */
  WIN_SIG			/* vittoria */
} signals;

#endif
