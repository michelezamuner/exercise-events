#include "app.h"

#include <stdio.h>

void app_ctor (app *this, evtMng *mng)
{
  sm_ctor (&(this->core), app_dispatch);
  this->mng = mng;
}

void app_dispatch (sm *this, const event *e)
{
  static const event exit_confirmed_evt = { EXIT_CONFIRMED_SIG };
  const event *new_e = (event *)0;
  
  switch (e->sig)
  {
    case EXIT_SIG:
    {
      printf ("elaborazioni finali in corso..\n");
      new_e = &exit_confirmed_evt;
      break;
    }
  }
  
  if (new_e != (event *)0)
  {
    evtMng_pushEvt (((app *)this)->mng, &new_e);
  }
}
