#ifndef APP_H
#define APP_H

#include "../events/evtMng.h"

typedef struct _app
{
  sm core;
  evtMng *mng;
} app;

void app_ctor (app *, evtMng *);
void app_dispatch (sm *, const event *);

#endif
