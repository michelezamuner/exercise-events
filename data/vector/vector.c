#include <string.h>
#include <stdlib.h>

#include "vector.h"

#define VECTOR_INIT_SIZE		0
#define VECTOR_INIT_CAPACITY		4
#define VECTOR_GROW_FACTOR		2
#define VECTOR_POS(index)		(this->elems + this->elem_size * (index))

static void vector_grow (vector *);

void vector_ctor (vector *this, uint16_t elem_size)
{
  this->elem_size = elem_size;
  this->size = VECTOR_INIT_SIZE;
  this->capacity = VECTOR_INIT_CAPACITY;
  this->elems = calloc (VECTOR_INIT_CAPACITY, elem_size);
}

void vector_push (vector *this, const void *elem)
{
  if (!elem) return;
  
  if (this->size == this->capacity)
  {
    vector_grow (this);
  }

  memcpy (VECTOR_POS(this->size), elem, this->elem_size);
  this->size++;
}

void * vector_get (const vector *this, uint16_t index)
{
  if (index >= this->size) return NULL;
  
  return VECTOR_POS(index);
}

uint16_t vector_elem_size (const vector *this)
{
  return this->elem_size;
}
uint16_t vector_size (const vector *this)
{
  return this->size;
}
uint16_t vector_capacity (const vector *this)
{
  return this->capacity;
}

static void vector_grow (vector *this)
{
  this->capacity *= VECTOR_GROW_FACTOR;
  this->elems = realloc (this->elems, this->elem_size * this->capacity);
} 
