#include "vector.h"

#include <stdio.h>

#define V_GET(index)		(*((number **)vector_get (v, (index))))
#define V_GET1(index)		((number *)vector_get (v, (index)))

/* classe base astratta (macchina a stati) */
typedef struct _sm
{
  void (*dispatch)(struct _sm *);
} sm;

void sm_ctor (sm *me, void (*dispatch)(sm *))
{
  me->dispatch = dispatch;
}

void sm_dispatch (sm *me)
{
  me->dispatch (me);
}

/* classe derivata (applicazione) */
typedef struct _app
{
  sm core;
  int i;
} app;

void app_dispatch (sm *);
int app_i (app *);

void app_ctor (app *me, int i)
{
  sm_ctor (&(me->core), app_dispatch);
  me->i = i;
}

void app_dispatch (sm *me)
{
  printf ("app %d\n", app_i ((app *)me));
}

int app_i (app *me)
{
  return me->i;
}

/* classe derivata (interfaccia) */
typedef struct _ui
{
  sm core;
  int i;
} ui;

void ui_dispatch (sm *);
int ui_i (ui *);

void ui_ctor (ui *me, int i)
{
  sm_ctor (&(me->core), ui_dispatch);
  me-> i = i;
}

void ui_dispatch (sm *me)
{
  printf ("ui %d\n", ui_i ((ui *)me));
}

int ui_i (ui *me)
{
  return me->i;
}


typedef struct _number
{
  int i;
} number;

/* stampa un vettore di puntatori a number*/
void print_vector (vector *v)
{
  int i;
  printf ("ELEM_SIZE: %d\nSIZE: %d\nCAPACITY: %d\n\t", 
    (int)vector_elem_size (v), vector_size (v), vector_capacity (v)
  );
  
  for (i = 0; i < vector_size (v); i++)
  {
    printf ("%d", V_GET(i)->i);
    (i == v->size -1) ? printf ("\n") : printf ("\t");
  }
}

/* stampa un vettore di number */
void print1_vector (vector *v)
{
  int i;
  printf ("ELEM_SIZE: %d\nSIZE: %d\nCAPACITY: %d\n\t", 
    (int)vector_elem_size (v), vector_size (v), vector_capacity (v)
  );
  
  for (i = 0; i < vector_size (v); i++)
  {
    printf ("%d", V_GET1(i)->i);
    (i == v->size -1) ? printf ("\n") : printf ("\t");
  }
}

int main ()
{
  /* alloca e visualizza un vettore di puntatori a number */
  vector *v = malloc (sizeof (vector));
  vector_ctor (v, sizeof (number *));
  
  int i;
  for (i = 0; i < 10; i++)
  {
    number *n = malloc (sizeof (number)); n->i = i;
    vector_push (v, &n);
  }
  
  print_vector (v);
  
  
  /* alloca e visualizza un vettore di number */
  vector *v1 = malloc (sizeof (vector));
  vector_ctor (v1, sizeof (number));
  
  for (i = 0; i < 10; i++)
  {
    number *n = malloc (sizeof (number)); n->i = i;
    vector_push (v1, n);
  }
  
  print1_vector (v1);
  
  /* definendo un vettore di sm, il vettore allocherà spazio per oggetti
   * di dimensione pari a quella di sm, cioè della classe base. Se poi cerco
   * di inserire in quel vettore delle classi derivate da sm, non ci sarà spazio
   * per i loro membri, ma solo per i loro nuclei di tipo sm */
  
  /* nella programmazione ad oggetti, una classe come sm sarebbe una classe
   * base astratta, che non può essere istanziata, e che di conseguenza non
   * può essere il tipo degli elementi di un vector (vector<sm> sarebbe
   * illegale). Anche in C++, dunque, sarebbe necessario definire un 
   * vector <sm *>, e riempirlo con puntatori a sm, sui quali si potrà,
   * dereferenziandoli, invocare la funzione virtuale */
  
  /* alloca e visualizza un vettore di puntatori a sm */
  vector machines; vector_ctor (&machines, sizeof (sm *));
  
  
  for (i = 0; i < 5; i++)
  {
    app *a = malloc (sizeof (app)); app_ctor (a, 2*i);
    /* passaggio per riferimento di un puntatore ad a */
    vector_push (&machines, &a);
  
    ui *u = malloc (sizeof (ui)); ui_ctor (u, 2*i+1);
    /* passaggio per riferimento di un puntatore ad u */
    vector_push (&machines, &u);
  }
  
  printf ("ELEM SIZE %d\n SIZE %d\n CAPACITY %d\n",
	  (int)vector_elem_size (&machines), vector_size (&machines),
	  vector_capacity (&machines));
  for (i = 0; i < vector_size (&machines); i++)
  {
    sm_dispatch (*(sm **)vector_get (&machines, i));
  }
  
  return 0;
}
