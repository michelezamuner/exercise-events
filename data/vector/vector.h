#ifndef VECTOR_H
#define VECTOR_H

#include <stdint.h>

typedef struct _vector
{
  void *elems;
  uint16_t elem_size;
  uint16_t size;
  uint16_t capacity;
} vector;

void vector_ctor (vector *, uint16_t);

void vector_push (vector *, const void *);
void * vector_get (const vector *, uint16_t);

uint16_t vector_elem_size (const vector *);
uint16_t vector_size (const vector *);
uint16_t vector_capacity (const vector *);

#endif
