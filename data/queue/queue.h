#ifndef QUEUE_H
#define QUEUE_H

#include <stdint.h>
#include "bool.h"

typedef struct _node
{
  void *elem;
  struct _node *next;
} node;

typedef struct _queue
{
  uint16_t elem_size;
  node *first;
  node *last;
} queue;

void queue_ctor (queue *, uint16_t);
void queue_push (queue *, const void *);
void * queue_pop (queue *);
bool queue_isEmpty (const queue *); 

#endif