#include <stdio.h>
#include "queue.h"

int main ()
{
  queue *q;
  q = malloc (sizeof (queue));
  queue_ctor (q, sizeof (int));
  
  int i;
  for (i = 0; i < 10; i++)
  {
    int *n = malloc (sizeof (int)); *n = i;
    queue_push (q, n);
  }
  
  while (!queue_isEmpty (q))
  {
    printf ("%d\n", *(int *)queue_pop (q));
  }
}