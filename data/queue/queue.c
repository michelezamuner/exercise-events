#include <string.h>
#include <stdlib.h>

#include "queue.h"

void queue_ctor (queue *this, uint16_t elem_size)
{
  this->elem_size = elem_size;
  this->first = NULL;
  this->last = NULL;
}

void queue_push (queue *this, const void *elem)
{
  node *n = malloc (sizeof (node));
  n->next = NULL;
  n->elem = malloc (sizeof (this->elem_size));
  memcpy (n->elem, elem, this->elem_size);
  
  if (this->first == NULL)
  {
    this->first = n;
    this->last = n;
  } else
  {
    this->last->next = n;
    this->last = n;
  }
}

void * queue_pop (queue *this)
{
  void *elem = this->first->elem;
  node *t = this->first;
  
  this->first = this->first->next,  
  free (t);

  return elem;
}

bool queue_isEmpty (const queue *this)
{
  return this->first == NULL ? TRUE : FALSE;
} 
