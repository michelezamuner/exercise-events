#ifndef BOOL_H
#define BOOL_H

typedef enum bool
{
  FALSE,
  TRUE
} bool;

#endif
