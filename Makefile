OBJECTS = main.o ui.o app.o termutils.o evtMng.o sm.o vector.o queue.o

tilex : $(OBJECTS)
	@gcc $(OBJECTS) -o tilex

main.o : main.c ui/ui.h
	@gcc -c main.c -o main.o

ui.o : ui/ui.c ui/ui.h events/evtMng.h ui/termutils/termutils.h
	@gcc -c ui/ui.c -o ui.o

app.o : app/app.c app/app.h events/evtMng.h
	@gcc -c app/app.c -o app.o

termutils.o : ui/termutils/termutils.c ui/termutils/termutils.h
	@gcc -c ui/termutils/termutils.c -o termutils.o

evtMng.o : events/evtMng.c events/evtMng.h events/sm.h \
	   data/vector/vector.h data/queue/queue.h
	@gcc -c events/evtMng.c -o evtMng.o

sm.o : events/sm.c events/sm.h
	@gcc -c events/sm.c -o sm.o

vector.o : data/vector/vector.c data/vector/vector.h
	@gcc -c data/vector/vector.c -o vector.o

queue.o : data/queue/queue.c data/queue/queue.h
	@gcc -c data/queue/queue.c -o queue.o

.PHONY : clean
clean :
	@rm *.o tilex
