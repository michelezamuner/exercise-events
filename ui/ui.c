#include "ui.h"

#define INIT_STATE		MENU

static void ui_getEvt (const ui *);

void ui_ctor (ui *this, evtMng *mng)
{
  sm_ctor (&(this->core), ui_dispatch);
  this->mng = mng;
  ((sm *)this)->state = INIT_STATE;
  init_term ();
}

void ui_dispatch (sm *this, const event *e)
{
  static const event exit_evt = { EXIT_SIG };
  const event *new_e = (event *)0;
  
  switch (e->sig)
  {
    /* E' stato richiesto il controllo della presenza di eventuali
     * eventi esterni */
    case QUERY_EVENT_SIG:
    {
      ui_getEvt ((ui *)this);
      break;
    }
    
    /* E' stato premuto il tasto 'n' */
    case N_SIG:
    {
      printf ("Premuto n!!\n");
      break;
    }
    
    /* E' stato premuto il tasto 'p' */
    case P_SIG:
    {
      printf ("Premuto p!!\n");
      break;
    }
    
    /* E' stato premuto il tasto 'w' */
    case W_SIG:
    {
      printf ("Premuto w!!\n");
      break;
    }
    
    /* E' stato premuto il tasto 'a' */
    case A_SIG:
    {
      printf ("Premuto a!!\n");
      break;
    }
    
    /* E' stato premuto il tasto 's' */
    case S_SIG:
    {
      printf ("Premuto s!!\n");
      break;
    }
    
    /* E' stato premuto il tasto 'd' */
    case D_SIG:
    {
      printf ("Premuto d!!\n");
      break;
    }
    
    /* E' stato premuto il tasto 'q' */
    case Q_SIG:
    {
      printf ("Q_SIG %d\n", this->state == CONFIRM_EXIT ? 1 : 0);
      if (this->state != CONFIRM_EXIT)
      {
	this->state = CONFIRM_EXIT;
	printf ("Sei sicuro di voler uscire? (Y/N) ");
      }
      break;
    }
    
    /* E' stato premuto il tasto 'y' */
    case Y_SIG:
    {
      switch (this->state)
      {
	case CONFIRM_EXIT:
	{
	  /* l'utente ha premuto 'y' quando l'interfaccia era nello stato
	   * CONFIRM_EXIT: significa che l'utente vuole davvero uscire, allora
	   * possiamo diramare la richiesta di uscita all'applicazione, in modo
	   * che possa svolgere le opportune elaborazioni prima dell'uscita dal
	   * programma */
	  new_e = &exit_evt;
	  break;
	}
      }
    }
    
    /* La richiesta di uscita è stata confermata dall'applicazione */
    case EXIT_CONFIRMED_SIG:
    {
      /* se arriva l'evento EXIT_CONFIRMED_SIG vuol dire che la richiesta di uscita
       * è stata accettata, e si può terminare l'applicazione */
      printf ("Bye bye!\n");
      exit_term (0);
      break;
    }
  }
  
  if (new_e != (event *)0)
  {
      evtMng_pushEvt (((ui *)this)->mng, &new_e);
  }
}

void ui_getEvt (const ui *this)
{
  if (kbhit ())		/* se la tastiera è stata premuta */
  {
    static const event n_evt = { N_SIG };
    static const event p_evt = { P_SIG };
    static const event w_evt = { W_SIG };
    static const event a_evt = { A_SIG };
    static const event s_evt = { S_SIG };
    static const event d_evt = { D_SIG };
    static const event q_evt = { Q_SIG };
    static const event y_evt = { Y_SIG };
    const event *e = (event *)0;
    
    switch (getchar ())
    {
      case 'n': case 'N':
      {
	e = &n_evt;
	fflush (stdout);
	break;
      }
      case 'p': case 'P':
      {
	e = &p_evt;
	fflush (stdout);
	break;
      }
      case 'w': case 'W':
      {
	e = &w_evt;
	fflush (stdout);
	break;
      }
      case 'a': case 'A':
      {
	e = &a_evt;
	fflush (stdout);
	break;
      }
      case 's': case 'S':
      {
	e = &s_evt;
	fflush (stdout);
	break;
      }
      case 'd': case 'D':
      {
	e = &d_evt;
	fflush (stdout);
	break;
      }
      case 'q': case 'Q':
      {
	e = &q_evt;
	fflush (stdout);
	break;
      }
      case 'y': case 'Y':
      {
	e = &y_evt;
	fflush (stdout);
	break;
      }
    }
    
    if (e != (event *)0)
    {
      evtMng_pushEvt (this->mng, &e);
    }
  }
}
