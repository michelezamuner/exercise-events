#ifndef UI_H
#define UI_H

#include <stdio.h>
#include "../events/evtMng.h"
#include "./termutils/termutils.h"

typedef enum ui_state
{
  MENU,				/* stato menu: comincia una nuova partita, carica, ecc..*/
  CONFIRM_EXIT,			/* sei sicuro di voler uscire? */
  SELECTED,			/* tessera selezionata */
  SELECTION_REQUESTED,		/* stai cercando di selezionare un'altra tessera */
  READY,			/* pronto a muovere */
  MOVE_REQUESTED		/* stai cercando di muovere una tessera */
} ui_state;

typedef struct _ui
{
  sm core;
  evtMng *mng;
} ui;

void ui_ctor (ui *, evtMng *);			/* costruisce la macchina */
void ui_dispatch (sm *, const event *);		/* risponde all'evento */

#endif