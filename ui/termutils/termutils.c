#include "termutils.h"

#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include <sys/time.h>

static struct termios stored_settings;

void set_keypress()
{
	struct termios new_settings;

	tcgetattr(0, &stored_settings);

	new_settings = stored_settings;

	/* Disable canonical mode, and set buffer size to 1 byte */
	new_settings.c_lflag &= (~ICANON);
	new_settings.c_cc[VTIME] = 0;
	new_settings.c_cc[VMIN] = 1;

	tcsetattr(0,TCSANOW,&new_settings);
}

void reset_keypress()
{
	tcsetattr(0,TCSANOW,&stored_settings);
	return;
}

void echo_off()
{
	struct termios new_settings;
	tcgetattr(0, &stored_settings);
	new_settings = stored_settings;
	new_settings.c_lflag &= (~ECHO);
	tcsetattr(0,TCSANOW,&new_settings);
}

void echo_on()
{
	tcsetattr(0, TCSANOW, &stored_settings);
}

int kbhit()
{
	struct timeval tv;
	fd_set read_fd;

	tv.tv_sec = 0;
	tv.tv_usec = 0;
	FD_ZERO(&read_fd);
	FD_SET(0,&read_fd);

	if(select(1, &read_fd, NULL, NULL, &tv) == -1)
		return 0;

	if(FD_ISSET(0, &read_fd))
		return 1;

	return 0;
}

void init_term()
{
  set_keypress();
  echo_off();
}

void exit_term(int error)
{
	reset_keypress();
	echo_on();
	_exit(error);
}
