#ifndef TERMUTILS_H
#define TERMUTILS_H

void set_keypress();	/* inserisce i caratteri nel buffer senza aspettare INVIO */

void reset_keypress();		/* torna alla modalità canonica */

void echo_off();	/* non mostra sulla console il carattere del tasto premuto */

void echo_on();			/* ripristina la visualizzazione dei caratteri */

int kbhit();		/* controlla se la tastiera ha ricevuto pressioni */

void init_term();			/* keypress + echo_off */

void exit_term(int error);	/* esce ripristinando keypress e echo */

#endif
