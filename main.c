#include <stdlib.h>

#include "ui/ui.h"
#include "app/app.h"

static evtMng *mng;
static ui *intf;
static app *application;

int main ()
{
  mng = malloc (sizeof (evtMng)); evtMng_ctor (mng);
  intf = malloc (sizeof (ui)); ui_ctor (intf, mng);
  application = malloc (sizeof (app)); app_ctor (application, mng);
  
  evtMng_pushSm (mng, (const sm **)&intf);
  evtMng_pushSm (mng, (const sm **)&application);
  
  evtMng_start (mng);
  
  return 0;
}
